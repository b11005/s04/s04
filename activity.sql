-- Find all artists that has letter D on it's name.
SELECT * FROM artists WHERE name LIKE "%d%" OR "%d" OR "d%";

-- Find all songs that has a length of less than 230.
 SELECT * FROM songs WHERE length < 230;

-- Join albums and song (ONLY show the album_name, song_name, and length)
SELECT albums.album_title

FROM albums 

JOIN songs

ON songs.song_name =
songs.length;

-- Join the 'artists' and 'albums' tables (find all albums that has letter A on its name)
SELECT * FROM artists 
	JOIN albums
		WHERE album_title LIKE "%A%" OR "%A" OR "A%";

-- Sort the albums in Z-A order (show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the 'albums' and 'songs' tables (Sort albums from Z-A and sort songs from A-Z)
SELECT album_title, song_name FROM albums, songs
GROUP BY albums.album_title, songs.song_name
ORDER BY albums.album_title DESC, songs.song_name ASC